package com.nasa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackDockerApplication.class, args);
	}

}
