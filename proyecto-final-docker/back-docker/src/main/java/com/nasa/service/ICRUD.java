package com.nasa.service;

import java.util.List;

public interface ICRUD<T,ID> {
	
	List<T> findAll() throws Exception;

	T create(T model) throws Exception;

	T findById(ID id) throws Exception;
	
	T update(T model) throws Exception;

	void deleteById(ID id) throws Exception;

}
