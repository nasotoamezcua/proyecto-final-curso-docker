package com.nasa.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nasa.model.Producto;
import com.nasa.repo.IProductoRepo;
import com.nasa.service.IProductoService;

@Service
public class ProductoServiceImpl implements IProductoService {
	
	@Autowired
	private IProductoRepo repo;

	@Override
	public List<Producto> findAll() throws Exception {
		return repo.findAll();
	}

	@Override
	public Producto create(Producto model) throws Exception {
		return repo.save(model);
	}

	@Override
	public Producto findById(Integer id) throws Exception {
		Optional<Producto> op = repo.findById(id);
		return op.isPresent() ? op.get(): new Producto();
	}

	@Override
	public Producto update(Producto model) throws Exception {
		return repo.save(model);
	}

	@Override
	public void deleteById(Integer id) throws Exception {
		repo.deleteById(id);
	}
}
