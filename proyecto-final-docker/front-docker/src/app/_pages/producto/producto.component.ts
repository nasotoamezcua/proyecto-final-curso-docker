import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatSnackBar, MatDialog } from '@angular/material';
import { Producto } from 'src/app/_model/producto';
import { ProductoService } from './../../_service/producto.service';
import { ProductoDialogoComponent } from './producto-dialogo/producto-dialogo.component';
import { switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  msgAviso: string = 'AVISO';
  msgElimino: string = 'Se elimino';
  displayedColumns = ['idProducto', 'nombre', 'marca', 'acciones'];
  dataSource: MatTableDataSource<Producto>

  @ViewChild(MatSort, {static:true}) sort : MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private snackBar: MatSnackBar,
    private doalogo: MatDialog,
    private productosService: ProductoService) { }

  ngOnInit() {
   this.productosService.productoCambio.subscribe(data =>{
     this.dataSource = new MatTableDataSource(data);
     this.dataSource.sort = this.sort;
     this.dataSource.paginator = this.paginator;
   });

   this.productosService.mensajeCambio.subscribe(data =>{
     this.snackBar.open(data, this.msgAviso,{
       duration: 2000
     } );
   });


   this.productosService.listar().subscribe(data =>{
     this.dataSource = new MatTableDataSource(data);
     this.dataSource.sort = this.sort;
     this.dataSource.paginator = this.paginator;
   });
  }

  filtrar(valor: string){
    this.dataSource.filter = valor.trim().toLocaleLowerCase();
  }

  abrirDialogo(producto ? : Producto){
    let product = producto != null ? producto : new Producto();
    this.doalogo.open(ProductoDialogoComponent, {
      width: '250px',
      data: product
    });
  }

  eliminar(producto: Producto){
   this.productosService.eliminar(producto.idProducto).pipe( switchMap(  ()=>{
     return this.productosService.listar();
   } )).subscribe(data =>{
     this.productosService.productoCambio.next(data);
     this.productosService.mensajeCambio.next(this.msgElimino);
   });

  }

}
