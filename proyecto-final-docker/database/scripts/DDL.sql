CREATE DATABASE IF NOT EXISTS db_docker CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE db_docker;
DROP TABLE IF EXISTS producto;

CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(15) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  PRIMARY KEY (`id_producto`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
